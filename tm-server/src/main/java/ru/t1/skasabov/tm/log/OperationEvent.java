package ru.t1.skasabov.tm.log;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class OperationEvent {

    @NotNull
    private OperationType type;

    @NotNull
    private Object entity;

    @Nullable
    private String table;

    @NotNull
    private Long timestamp = System.currentTimeMillis();

    public OperationEvent(@NotNull final OperationType type, @NotNull final Object entity) {
        this.type = type;
        this.entity = entity;
    }

}
