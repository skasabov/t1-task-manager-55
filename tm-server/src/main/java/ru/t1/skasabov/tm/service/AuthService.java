package ru.t1.skasabov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.skasabov.tm.api.service.IAuthService;
import ru.t1.skasabov.tm.api.service.IPropertyService;
import ru.t1.skasabov.tm.api.service.dto.ISessionDTOService;
import ru.t1.skasabov.tm.api.service.dto.IUserDTOService;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.exception.field.LoginEmptyException;
import ru.t1.skasabov.tm.exception.field.PasswordEmptyException;
import ru.t1.skasabov.tm.exception.user.AuthenticationException;
import ru.t1.skasabov.tm.dto.model.SessionDTO;
import ru.t1.skasabov.tm.dto.model.UserDTO;
import ru.t1.skasabov.tm.util.CryptUtil;
import ru.t1.skasabov.tm.util.HashUtil;

import java.util.Date;

@Service
@NoArgsConstructor
public final class AuthService implements IAuthService {

    @NotNull
    @Autowired
    private IUserDTOService userService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ISessionDTOService sessionService;

    @NotNull
    @Override
    public UserDTO registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public UserDTO registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        return userService.create(login, password, role);
    }

    @NotNull
    @Override
    public String login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        if (user.getLocked()) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AuthenticationException();
        if (!hash.equals(user.getPasswordHash())) throw new AuthenticationException();
        return getToken(user);
    }

    @Override
    public void invalidate(@Nullable final SessionDTO session) {
        if (session == null) return;
        sessionService.removeOne(session);
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO validateToken(@Nullable final String token) {
        if (token == null) throw new AuthenticationException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AuthenticationException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final SessionDTO session = objectMapper.readValue(json, SessionDTO.class);

        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AuthenticationException();
        return session;
    }

    @NotNull
    private String getToken(@NotNull final UserDTO user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@Nullable final SessionDTO session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private SessionDTO createSession(@NotNull final UserDTO user) {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return sessionService.add(session);
    }

}
