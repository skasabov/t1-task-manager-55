package ru.t1.skasabov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;

import java.util.Date;
import java.util.List;

public interface IProjectDTOService extends IUserOwnedDTOService<ProjectDTO> {

    @NotNull
    List<ProjectDTO> findAll(@Nullable Sort sortType);

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sortType);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name, @Nullable String description,
            @Nullable Date dateBegin, @Nullable Date dateEnd
    );

    @NotNull
    ProjectDTO updateById(
            @Nullable String userId,
            @Nullable String id, @Nullable String name, @Nullable String description
    );

    @NotNull
    ProjectDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index, @Nullable String name, @Nullable String description
    );

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    ProjectDTO changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}
