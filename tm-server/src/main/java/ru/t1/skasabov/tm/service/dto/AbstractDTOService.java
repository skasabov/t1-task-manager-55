package ru.t1.skasabov.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.skasabov.tm.api.service.dto.IDTOService;
import ru.t1.skasabov.tm.dto.model.AbstractModelDTO;

@Service
@NoArgsConstructor
public abstract class AbstractDTOService<M extends AbstractModelDTO> implements IDTOService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

}
