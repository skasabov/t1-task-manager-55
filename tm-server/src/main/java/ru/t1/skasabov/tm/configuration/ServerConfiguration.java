package ru.t1.skasabov.tm.configuration;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.skasabov.tm.api.service.IPropertyService;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.SessionDTO;
import ru.t1.skasabov.tm.dto.model.TaskDTO;
import ru.t1.skasabov.tm.dto.model.UserDTO;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.Session;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("ru.t1.skasabov.tm")
public class ServerConfiguration {

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory(@NotNull final IPropertyService propertyService) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDatabaseDriver());
        settings.put(Environment.URL, propertyService.getDatabaseUrl());
        settings.put(Environment.USER, propertyService.getDatabaseUserName());
        settings.put(Environment.PASS, propertyService.getDatabasePassword());
        settings.put(Environment.DIALECT, propertyService.getDatabaseDialect());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDatabaseHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, propertyService.getDatabaseShowSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDatabaseCacheUseSecondLevelCache());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getDatabaseCacheUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getDatabaseCacheUseMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getDatabaseCacheRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDatabaseCacheProviderConfigurationFileResourcePath());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getDatabaseCacheRegionFactoryClass());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

    @Bean
    @NotNull
    @SneakyThrows
    @Scope("prototype")
    public Liquibase liquibase(@NotNull final IPropertyService propertyService) {
        @NotNull final Connection connection = DriverManager.getConnection(
                propertyService.getDatabaseUrl(),
                propertyService.getDatabaseUserName(),
                propertyService.getDatabasePassword()
        );
        @NotNull final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        @NotNull final Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
        @NotNull final ClassLoaderResourceAccessor accessor = new ClassLoaderResourceAccessor();
        return new Liquibase("changelog/changelog-master.xml", accessor, database);
    }

}
