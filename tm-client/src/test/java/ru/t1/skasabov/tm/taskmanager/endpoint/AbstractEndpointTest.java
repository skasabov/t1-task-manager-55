package ru.t1.skasabov.tm.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.skasabov.tm.configuration.ClientConfiguration;

public abstract class AbstractEndpointTest {

    @NotNull
    protected ApplicationContext context = new AnnotationConfigApplicationContext(ClientConfiguration.class);

}
