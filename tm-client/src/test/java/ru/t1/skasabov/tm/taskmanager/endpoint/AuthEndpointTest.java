package ru.t1.skasabov.tm.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.skasabov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.skasabov.tm.api.endpoint.IUserEndpoint;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.UserLoginResponse;
import ru.t1.skasabov.tm.dto.response.UserLogoutResponse;
import ru.t1.skasabov.tm.dto.response.UserProfileResponse;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.taskmanager.marker.SoapCategory;

import javax.xml.ws.WebServiceException;

public class AuthEndpointTest extends AbstractEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = context.getBean(IAuthEndpoint.class);

    @NotNull
    private final IUserEndpoint userEndpoint = context.getBean(IUserEndpoint.class);

    @Nullable
    private String token;

    @Nullable
    private String adminToken;

    @Before
    public void initTest() {
        userEndpoint.registryUser(
                new UserRegistryRequest("123", "123", "123@123")
        );
        userEndpoint.registryUserRole(
                new UserRoleRegistryRequest("456", "456", Role.ADMIN)
        );
        @NotNull final UserLoginResponse response = authEndpoint.login(
                new UserLoginRequest("123", "123")
        );
        token = response.getToken();
        @NotNull final UserLoginResponse adminResponse = authEndpoint.login(
                new UserLoginRequest("456", "456")
        );
        adminToken = adminResponse.getToken();
    }

    @Test
    @Category(SoapCategory.class)
    public void testLogin() {
        @NotNull final UserLoginRequest request = new UserLoginRequest("123", "123");
        @NotNull final UserLoginResponse response = authEndpoint.login(request);
        Assert.assertNotNull(response.getToken());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testLoginEmpty() {
        @NotNull final UserLoginRequest request = new UserLoginRequest("", "123");
        authEndpoint.login(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testLoginEmptyPassword() {
        @NotNull final UserLoginRequest request = new UserLoginRequest("123", "");
        authEndpoint.login(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testLoginIncorrect() {
        @NotNull final UserLoginRequest request = new UserLoginRequest("456", "123");
        authEndpoint.login(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testLoginIncorrectPassword() {
        @NotNull final UserLoginRequest request = new UserLoginRequest("123", "456");
        authEndpoint.login(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testLogout() {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest();
        request.setToken(token);
        @NotNull final UserLogoutResponse response = authEndpoint.logout(request);
        Assert.assertNotNull(response);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testLogoutNoAuth() {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest();
        authEndpoint.logout(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testProfile() {
        @NotNull final UserProfileRequest request = new UserProfileRequest();
        request.setToken(token);
        @NotNull final UserProfileResponse response = authEndpoint.profile(request);
        Assert.assertNotNull(response.getUser());
        Assert.assertEquals("123", response.getUser().getLogin());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testProfileNoAuth() {
        @NotNull final UserProfileRequest request = new UserProfileRequest();
        authEndpoint.profile(request);
    }

    @After
    public void endTest() {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest();
        request.setToken(token);
        authEndpoint.logout(request);
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest("123");
        removeRequest.setToken(adminToken);
        userEndpoint.removeUser(removeRequest);
        @NotNull final UserLogoutRequest adminLogoutRequest = new UserLogoutRequest();
        adminLogoutRequest.setToken(adminToken);
        authEndpoint.logout(adminLogoutRequest);
        @NotNull final UserRemoveRequest adminRemoveRequest = new UserRemoveRequest("456");
        adminRemoveRequest.setToken(adminToken);
        userEndpoint.removeUser(adminRemoveRequest);
    }

}
