package ru.t1.skasabov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.UserRoleRegistryRequest;
import ru.t1.skasabov.tm.dto.response.UserRoleRegistryResponse;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.util.TerminalUtil;

@Component
@NoArgsConstructor
public final class UserRoleRegistryCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-role-registry";

    @NotNull
    private static final String DESCRIPTION = "Registry user with role.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER ROLE REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER ROLE:");
        @NotNull final String roleValue = TerminalUtil.nextLine();
        @NotNull final Role role = Role.toRole(roleValue);
        @NotNull final UserRoleRegistryRequest request = new UserRoleRegistryRequest(login, password, role);
        @NotNull final UserRoleRegistryResponse response = userEndpoint.registryUserRole(request);
        showUser(response.getUser());
    }

}
