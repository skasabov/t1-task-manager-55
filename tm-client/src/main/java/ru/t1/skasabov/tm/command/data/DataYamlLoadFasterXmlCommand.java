package ru.t1.skasabov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.DataYamlLoadFasterXmlRequest;

@Component
public final class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-load-yaml";

    @NotNull
    private static final String DESCRIPTION = "Load data from yaml file.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD YAML]");
        @NotNull final DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest();
        request.setToken(getToken());
        domainEndpoint.loadDataYamlFasterXml(request);
    }

}
