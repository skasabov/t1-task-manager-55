package ru.t1.skasabov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.DataXmlLoadFasterXmlRequest;

@Component
public final class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-load-xml";

    @NotNull
    private static final String DESCRIPTION = "Load data from xml file.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXmlLoadFasterXmlRequest request = new DataXmlLoadFasterXmlRequest();
        request.setToken(getToken());
        domainEndpoint.loadDataXmlFasterXml(request);
    }

}
