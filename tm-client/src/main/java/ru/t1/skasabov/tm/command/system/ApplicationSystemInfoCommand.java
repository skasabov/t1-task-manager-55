package ru.t1.skasabov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.ApplicationSystemInfoRequest;
import ru.t1.skasabov.tm.dto.response.ApplicationSystemInfoResponse;

@Component
@NoArgsConstructor
public final class ApplicationSystemInfoCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "info";

    @NotNull
    private static final String DESCRIPTION = "Show system information.";

    @NotNull
    private static final String ARGUMENT = "-i";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final ApplicationSystemInfoRequest request = new ApplicationSystemInfoRequest();
        @NotNull final ApplicationSystemInfoResponse response = systemEndpoint.getSystemInfo(request);
        System.out.println("[INFO]");
        System.out.println("Available processors (cores): " + response.getAvailableProcessors());
        System.out.println("Free memory: " + response.getFreeMemory());
        System.out.println("Maximum memory: " + response.getMaximumMemory());
        System.out.println("Total memory: " + response.getTotalMemory());
        System.out.println("Usage memory: " + response.getUsageMemory());
    }

}
