package ru.t1.skasabov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.skasabov.tm.dto.response.ApplicationAboutResponse;

@Component
@NoArgsConstructor
public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "about";

    @NotNull
    private static final String DESCRIPTION = "Show about program.";

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse response = systemEndpoint.getAbout(request);
        System.out.println("name: " + response.getName());
        System.out.println("email: " + response.getEmail());
    }

}
