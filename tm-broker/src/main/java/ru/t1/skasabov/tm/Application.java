package ru.t1.skasabov.tm;

import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class Application {

    @SneakyThrows
    public static void main(@Nullable final String[] args) {
        @NotNull final BrokerService broker = new BrokerService();
        broker.addConnector("tcp://0.0.0.0:61616");
        broker.start();
    }

}
