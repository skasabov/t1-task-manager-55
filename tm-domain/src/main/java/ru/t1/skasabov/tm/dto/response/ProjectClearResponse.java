package ru.t1.skasabov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectClearResponse extends AbstractResponse {

    @NotNull
    private List<ProjectDTO> projects = new ArrayList<>();

    public ProjectClearResponse(@NotNull final List<ProjectDTO> projects) {
        this.projects = projects;
    }

}
